# Epipolar scan

This is a photography project in which an object is rotated on a turntable, and a circular epipolar volume of pixels is collected.

Slicing the volume in various ways creates intriguing visuals.

## Multimeter

![](data/multimeter.jpg)

### rotate

![](data/multimeter_rotate.mp4)

### scan

![](data/multimeter_scan.mp4)

## Beaker

![](data/beaker.jpg)

### rotate

![](data/beaker_rotate.mp4)

### scan

![](data/beaker_scan.mp4)
